﻿
/// <summary>
/// 对应面板的固定名称
/// </summary>
public class UIName
{
    #region 推演场景
    /// <summary>
    /// 主界面
    /// </summary>
    public const string Main = "MainPanel";
    /// <summary>
    /// 实体管理界面
    /// </summary>
    public const string Entities = "EntitiesPanel";
    /// <summary>
    /// 添加编组界面
    /// </summary>
    public const string Group = "GroupPanel";
    /// <summary>
    /// 甘特图界面
    /// </summary>
    public const string TimeLine = "TimeLinePanel";
    /// <summary>
    /// 地图工具界面
    /// </summary>
    public const string TerrainTool = "TerrainToolPanel";
    #endregion

    #region 地图编辑场景
    public const string TerrainModifier = "TerrainModifierPanel";
    #endregion

    #region 编组编辑场景
    /// <summary>
    /// 所有编组管理界面
    /// </summary>
    public const string AllGroups = "AllGroupsPanel";
    /// <summary>
    /// 编辑编组界面
    /// </summary>
    public const string EditGroup = "EditGroupPanel";
    #endregion
    
    /// <summary>
    /// 设置界面
    /// </summary>
    public const string Setting = "SettingPanel";
}
